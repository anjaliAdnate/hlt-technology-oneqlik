import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDocPage } from './add-doc';

@NgModule({
  declarations: [
    AddDocPage,
  ],
  imports: [
    IonicPageModule.forChild(AddDocPage),
  ],
})
export class AddDocPageModule {}
