import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeedRepoPage } from './speed-repo';
import { SelectSearchableModule } from 'ionic-select-searchable';
@NgModule({
  declarations: [
    SpeedRepoPage,
  ],
  imports: [
    IonicPageModule.forChild(SpeedRepoPage),
    SelectSearchableModule
  ],
})
export class SpeedRepoPageModule {}
