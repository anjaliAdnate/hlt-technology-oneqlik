import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDeviceModalPage } from './add-device-modal';

@NgModule({
  declarations: [
    AddDeviceModalPage
  ],
  imports: [
    IonicPageModule.forChild(AddDeviceModalPage)
  ]
})
export class AddDeviceModalPageModule {}