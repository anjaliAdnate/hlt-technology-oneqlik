import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelReportPage } from './fuel-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    FuelReportPage,
  ],
  imports: [
    IonicPageModule.forChild(FuelReportPage),
    SelectSearchableModule
  ],
})
export class FuelReportPageModule {}
